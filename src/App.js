import './App.css';
import InputData from './Component/InputData';


require('flowbite/plugin')
function App() {
  return (
    <div className="App">
      <div className='min-h-screen bg-gradient-to-t from-gray-300 to-slate-900 '>
      <InputData/>
      </div>
      
    </div>
  );
}

export default App;
