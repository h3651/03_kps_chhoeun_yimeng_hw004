import React, { Component } from 'react'
import Swal from 'sweetalert2'
import "animate.css";


export default class extends Component {
  test =(item)=>{
    Swal.fire({
      title: "Id : "+item.id+"\nEmail: "+item.email+"\nUsername : "+item.userName+"\nAge : "+item.age+"",
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    })
  }
  render() {
    return (
        <div>
        <div class="relative overflow-x-auto shadow-md ">
          
    <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-4">
        <table class="w-full text-sm text-left text-gray-500 ">
            <thead class="text-xs text-gray-100 uppercase bg-gray-800">
                <tr className=''>
                    <th scope="col" class="px-6 py-3 ">
                        ID
                    </th>
                    <th scope="col" class="px-6 py-3 ">
                        EMAIL
                    </th>
                    <th scope="col" class="px-6 py-3">
                        NAME
                    </th>
                    <th scope="col" class="px-6 py-3">
                        AGE
                    </th>
                    <th scope="col" class="px-6 py-3 text-center">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                
              {this.props.user.map((item) => (
                <tr class=" border-b odd:bg-white even:bg-pink-300" key={item.id}>
                  <td class="px-6 py-4 text-blue-900">{item.id}</td>
                  <td class="px-6 py-4 text-black">{item.email}</td>
                  <td class="px-6 py-4 text-black">{item.userName}</td>
                  <td class="px-6 py-4 text-black">{item.age}</td>
                  <td class="px-6 py-4 text-black">
                    <div className='flex flex-row space-x-1.5'>
                    <button onClick={()=>this.props.buttonId(item.id)} className={`w-28 h-11 rounded-md hover:bg-gray-300 focus:ring-2 font-bold text-black mx-auto mt-2 ${item.status=="Pending"?"bg-red-600 rounded-lg":"bg-green-500 rounded-md"}`}> {item.status}</button>
                    {/* showmorebutton */}
                    <a href="#_" onClick={()=>this.test(item)}
              class="mt-2 h-11 px-5 py-2.5 relative rounded group font-medium text-white  inline-block">
                    <span class="absolute top-0 left-0 w-full h-full rounded opacity-50 filter blur-sm bg-gradient-to-br from-purple-600 to-blue-500"></span>
                    <span class="h-full w-full inset-0 absolute mt-0.5 ml-0.5 bg-gradient-to-br filter group-active:opacity-0 rounded opacity-50 from-purple-600 to-blue-500"></span>
                    <span class="absolute inset-0 w-full h-full transition-all duration-200 ease-out rounded shadow-xl bg-gradient-to-br filter group-active:opacity-0 group-hover:blur-sm from-purple-600 to-blue-500"></span>
                    <span class="absolute inset-0 w-full h-full transition duration-200 ease-out rounded bg-gradient-to-br to-purple-600 from-blue-500"></span>
                    <span class="relative">Show More</span>
                  </a>
                    </div>
                    <div className='flex flex-row'>
                    </div>
                    <div>
                    
                    </div>
                  </td>
                </tr>
              ))}
            
            </tbody>
        </table>
    </div>

        </div>
      </div>
    )
  }
}
