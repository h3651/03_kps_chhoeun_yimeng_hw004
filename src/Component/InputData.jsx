import React, { Component } from 'react'
import "flowbite";
import TableData from './TableData';
import mail from '../Images/mail.png'

export default class InputData extends Component {
    constructor(){
        super();
        this.state={
            userData:[
                    { id:1, email : "loki002@gmail.com", userName : "Navy", age : 22, status : "Pending"} ,
                    { id:2, email : "dane003@gmail.com", userName: "Dane", age:25, status: "Pending" },
                    { id:3, email: "long532@gmail.com", userName: "Long", age: 34, status: "Pending"},
            ],
            newEmail: "null",
            newName: "null",
            newAge: "null",
            newStatus: "Pending",
        };
    }
    handleChangeEmail=(event)=>{
        this.setState({
            newEmail:event.target.value,
        });
    }
    handleChangeUserName=(event)=>{
        this.setState({
            newName:event.target.value,
        });
    }
    handleChangeAge=(event)=>{
        this.setState({
            newAge:event.target.value,
        })
    }
    handleInput=(e)=>{
        e.preventDefault();
        const newObj={
            id:this.state.userData.length+1,
            email:this.state.newEmail,
            userName:this.state.newName,
            age:this.state.newAge,
            status:this.state.newStatus,
        };
        console.log(newObj)
        this.setState(
            {
                userData:[...this.state.userData, newObj],   
                newEmail: "null",
                newName: "null",
                newAge: "null",
                newStatus: "Pending",         
            },
        )
    }
    getFromId=(id)=>{
        console.log(id);
        this.state.userData.map((e) => {
            if(e.id ==id){
                e.status == "Pending" ? (e.status="Done"):(e.status="Pending")
            }
        })
    this.setState({
        userData:this.state.userData
    })
    }

  render() {
    return (
        <div>
        <div class="flex lg:py-0 flex-row justify-center ">
            {/* fill the information */}
            <div class="w-full bg-gradient-to-r from-fuchsia-500 to-cyan-500 rounded-lg shadow dark:border md:mt-11 sm:max-w-md xl:p-0  ">
                <div class="p-6 space-y-4 md:space-y-6 sm:p-8">
                    <h1 class="text-xl font-bold leading-tight tracking-tight bg-gradient-to-r from-slate-800 to-rose-900 bg-clip-text text-transparent md:text-2xl ">
                        Please Fill Your Information 
                    </h1>
                    <form class="space-y-4 md:space-y-6" action="#">
                        <div>
                            <label for="email" class="block mb-2 text-sm  text-gray-900 font-extrabold">Your email  </label>
                            <input type="email" name="email" id="email" class="bg-gray-200 border border-gray-800 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  " placeholder=" name@gmail.com" onChange={this.handleChangeEmail}/>
                        </div>
                        <div className='flex flex-row justify-between space-x-1.5'>
                        <div>
                            <label for="username" class="block mb-2 text-sm  text-gray-900 font-extrabold ">Username</label>
                            <input type="text" name="username" id="username" placeholder=" name" class="bg-gray-200 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  " onChange={this.handleChangeUserName}/>
                        </div>
                        <div>
                            <label for="age" class="block mb-2 text-sm  text-gray-900 font-extrabold">Age</label>
                            <input type="text" name="age" id="age" placeholder=" age" class="bg-gray-200 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5  " onChange={this.handleChangeAge}/>
                        </div>
                        </div>
                        <div className='flex flex-col mx-auto '>
                        <p class="text-sm  text-gray-800 font-bold">
                           Click to Insert Data <a href="#" class="font-medium text-primary-600 hover:underline dark:text-primary-500"></a>
                        </p>

                        {/* button submit */}
                        <div className='flex flex-row justify-center mt-2'>
                        <a href="#_" onClick={this.handleInput} class="relative inline-block text-lg group">
                        <span class="relative z-10 block px-5 py-3 overflow-hidden font-medium leading-tight text-green-800 transition-colors duration-300 ease-out border-2 border-gray-900 rounded-lg group-hover:text-green-600">
                        <span class="absolute inset-0 w-full h-full px-5 py-3 rounded-lg bg-gray-50"></span>
                        <span class="absolute left-0 w-48 h-48 -ml-2 transition-all duration-300 origin-top-right -rotate-90 -translate-x-full translate-y-12 bg-gray-900 group-hover:-rotate-180 ease group-active:opacity-5"></span>
                        <span class="relative">Register</span>
                        </span>
                        <span class="absolute bottom-0 right-0 w-full h-12 -mb-1 -mr-1 transition-all duration-200 ease-linear bg-gray-900 rounded-lg group-hover:mb-0 group-hover:mr-0" data-rounded="rounded-lg"></span>
                        </a>
                         </div>
                        </div>   
                        
                    </form>
                    
                </div>
            </div>               
      </div>

      <div className=' flex flex-row justify-center'>
        <TableData user={this.state.userData}  buttonId={this.getFromId}/>
        </div> 
      </div>
    )
  }
}
